---
layout: post
title: "[포즈사이트] 애니 포즈 및 크로키 연습 사이트 추천(크로키, 디자인..."
toc: true
---


## 동작 연습 사이트 추천

## 피규어몰
 - 다양한 구도(앞, 뒤, 좌, 우)와 디테일한 묘사가 되있기에 포즈연습하기가 좋음.
 - [애니추천](https://goldfish-inhale.com/culture/post-00002.html) 대부분 "게임"카테고리에 있는 본질 몸놀림 참고함
 

 1. 피규어몰
 http://www.figuremall.co.kr/
 

 2. 하비박스
 http://hobbybox.co.kr/
 

 3. 히어로타임
 https://herotime.co.kr/index.html
 

 4. 피규어팜
 https://www.figurefarm.net/
 

 

## 명망가 신체 위주 몸자세 사이트 (크로키에 도움)
 - 실지로 사람의 마음가짐 위주 사이트
 - 어섯 사이트 표 있음
 

 1. line-of-action(노출o)
 https://line-of-action.com/practice-tools/figure-drawing/
 

 2. senshistock
 https://www.adorkastock.com/sketch/#
 

 3. quickposes(노출o)
 https://quickposes.com/en
 

 4. posetrainer(3D 근육모형)
 https://pose-trainer.com/
 

 5. sketchdaily(노출o)
 http://reference.sketchdaily.net/en
 

 6. modelfactory
 - 마네킹 체위 자료집 사이트

 - 다양한 포즈와 정식 버전을 이용하려면 유료
 http://modelfactory.co.kr/1636_poses.htm
 

 

 7. femal anatomy for artist(노출o)
 https://www.female-anatomy-for-artist.com/
 

 8. anatomy360info(유료, 노출o)
 http://anatomy360.info/anatomy-scan-reference-dump/
 

 9. deviantart
 - 크로키 사진 모은 데비앙계정
 https://www.deviantart.com/jookpubstock/art/Inspiration-Mood-Practise-004-833378954
 

 10. kitasite
 - 손, 팔, 물팍 등 디테일 크로키 가능
 http://kitasite.net/kr/cgkoza/muscle/hand4/hand4.htm
 

 

 11. bodieinmotion
 https://www.bodiesinmotion.photo/
 

 

## 감 결부 사이트
 1. adobe color
 https://color.adobe.com/ko/create/color-wheel
 

 2. lol colors
 https://www.webdesignrankings.com/resources/lolcolors/
 

 3. color palette
 https://colorpalettes.net/color-palette-4298/
 

 4. colourlovers
 https://www.colourlovers.com/
 

 

## 사진 주제 사이트
 - 남근 초상, 기업체 로고 및 브랜드가 들어간 사진은 상업적 이용시 주의
 - 유료로 구매해야하는 이미지들도 있음
 - 출처는 기연히 작성 / 의상, 디자인 모사품 연결 주의
 - 모든 사이트는 "참고"할 뿐 본인이 자기 재해석해서 손수 작품으로 만들어내야함
 

 1. picjumbo
 https://picjumbo.com/
 

 2. pexels
 https://www.pexels.com/
 

 3. gettyimagesbank
 https://www.gettyimagesbank.com/

 

 4. pixabay
 https://pixabay.com/ko/photos/
 

 5. unsplash
 https://unsplash.com/

 

 6. flickr
 - 온라인 사진 감회 커뮤니티
 https://www.flickr.com/
 

## etc
 - 원본 회도 찾아주는 사이트
 http://www.tineye.com/
 

